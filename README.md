# Sample MVVM Architecture

This repository implements MVVM architecture using: 
+ Room as Local Database 
+ Retrofit for API call
+ Databinding 
+ ViewModel and LiveData 
+ Rxjava supports reactive Programming